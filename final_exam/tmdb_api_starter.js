// File: tmdb_api.js
var movie_cell_template = "<div class='col-sm-3 movie-cell'><div class='movie-poster'><img></div><div class='movie-title'></div></div>";
var search_term = "";
var search_base_url = "https://api.themoviedb.org/3/search/movie?";
var poster_base_url = "https://image.tmdb.org/t/p/w500";
var search_api_url = getSearchUrl();   // use this string to build your search from search_url and search_term
var api_key = "api_key=a04b5cc3dcdc23a0bbed3c26040b40bd&query=";


// use this function to concantenate (build) your search url
function getSearchUrl(queryString) {
  var url = search_base_url + api_key + search_term;
  return url;
}

// use this function to concantenate (build) your poster url
// function getPosterUrl(imageString) {
//   var url = "<img src= " + poster_base_url + url_ref ">";
//   return url;
// }

// Shorthand for $( document ).ready()
$(function() {
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 1: key input processing
  // ========================================================
  $('input').on({
    // keydown - backspace or delete processing only
    keydown: function(e) {
      var code = e.which;
      console.log('KEYDOWN-key:' + code);
      if(code == 8){
        $('input').remove('string');
      }
    },
    // keypress - everything else processing
    //important to catch 'enter' and the search button
    keypress: function(e) {
      var code = e.which;
      console.log('KEYPRESS-key:' + code);
      if(code == 13){
      search_term = input;
      console.log(search_term);
    }
    }
  });
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 2:  process search on omdb
  // ========================================================
  $('#get-movie-btn').on('click', api_search);
  // Do the actual search in this function
  function api_search(e) {
    // prepare search string url => search_api_url
    $.ajax({
      url: search_api_url,
      dataType: "jsonp",
      success: function(data) {
        render(data);   //create a way to render data
      },
      cache: false
    });
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
  // TASK 3: perform all movie grid rendering operations
  // ========================================================
  function render(data) {
    // get movie data from search results data
    var id = parsed_json["results"]["id"];
    var title = parsed_json["results"]["title"];
    var url_ref = parsed_json["results"]["poster_path"];
    // select the movie grid and dump the sample html (if any)

    // every group of 4 gets their own class=row
    // if((i % 4) === 0) ({// enter a new <div class="row"> }
    // $(rowselector).append(movie_cell_template);
    // utilize the movie_cell_template to append, then add data from
    // movie data results (parsed JSON)
  }
  // ========================================================
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
});
